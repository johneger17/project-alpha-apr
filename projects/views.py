from django.views.generic.list import ListView
from django.views.generic import DetailView, CreateView
from django.shortcuts import redirect

from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    context_object_name = "projects"
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    context_object_name = "projects"
    template_name = "projects/detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    context_object_name = "projects"
    fields = ["name", "description", "members"]
    template_name = "projects/create.html"

    def form_valid(self, form):
        item = form.save(commit=True)
        item.assignee = self.request.user
        item.save()
        return redirect("show_project", pk=item.assignee.id)
